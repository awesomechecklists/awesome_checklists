require_relative "lib/awesome_checklists/version"

Gem::Specification.new do |spec|
  spec.name        = "awesome_checklists"
  spec.version     = AwesomeChecklists::VERSION
  spec.authors     = ["Benjamin Behr"]
  spec.email       = ["benny@digitalbehr.de"]
  spec.homepage    = "https://awesomechecklists.com"
  spec.summary     = "Adds support for user defined tasks and checklists"
  spec.description = "awesomechecklists adds task and checklist definition and handling to your rails app."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/awesomechecklists/awesome_checklists"
  spec.metadata["changelog_uri"] = "https://gitlab.com/awesomechecklists/awesome_checklists/-/blob/main/CHANGELOG.md"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "rails", ">= 7.0.5"
end
